import time
from umqttsimple import MQTTClient
import ubinascii
import machine
from machine import Pin
import micropython
import network
import time
# Set the debug to None
import esp
esp.osdebug(None)
# Activate the garbage collector
import gc
gc.collect()

# create input pin for PIR sensor on GPIO5
ldr = Pin(5, Pin.IN)
# create output pin for status led on GPIO2 connected to onboard blue LED
led = Pin(2, Pin.OUT)
led.value(1)

ssid = 'REPLACE with your SSID'
password = 'REPLACE with your Wi-Fi credentials'
mqtt_server = 'MQTT Broker address'
#mqtt_port = # mqtt borker port number
# A unique ID is needed for MQTT messages
client_id = ubinascii.hexlify(machine.unique_id())
# The topic the ESP8266 is subscribed to
topic_sub = b'/devices/esp8266/0/#'
# The topic the ESP8266 will be publishing messages
topic_pub = b'/devices/esp8266/events'

# Hold the last time a message was sent
last_message = 0
# The time between each message sent
# Means a new message will be sent every 5 seconds
message_interval = 5
# A counter to be added to the message
counter = 0

station = network.WLAN(network.STA_IF)

station.active(True)
station.connect(ssid, password)

while station.isconnected() == False:
  pass

print('Connection successful')
print(station.ifconfig())
