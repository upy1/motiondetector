
# Callback function
def sub_cb(topic, msg):
  # Printing the topic and the received message
  print((topic, msg))
  # Turn on or off the onboard led based on the received message!
  if msg == b'enable':
    led.value(0)
    print('The sensor has been enabled!')
  elif msg == b'disable':
    led.value(1)
    print('The sensor has been disabled!')

# Connecting to the broker as well as to subscribe to a topic
def connect_and_subscribe():
  # Can access these variables throughout the code
  global client_id, mqtt_server, topic_sub
  # The parameters set on boot.py
  client = MQTTClient(client_id, mqtt_server, mqtt_port)
  client.set_callback(sub_cb)
  client.connect()
  client.subscribe(topic_sub)
  print('Connected to %s MQTT broker, subscribed to %s topic' % (mqtt_server, topic_sub))
  return client

# Call in case the board fails to connect to the broker
def restart_and_reconnect():
  print('Failed to connect to MQTT broker. Reconnecting...')
  time.sleep(10)
  machine.reset()

try:
  client = connect_and_subscribe()
except OSError as e:
  restart_and_reconnect()


# Receiving and publishing the messages here:
# to prevent the ESP from crashing in case something goes wrong the 'try' & 'except' blocks has been defined
while True:
  try:
    client.check_msg()
    if (not(led.value())) and (ldr.value()):
      msg = b'Movement Detected!'
      client.publish(topic_pub, msg)
      time.sleep(10)
  except OSError as e:
    restart_and_reconnect()
